/************************************************************************************** 
  	This file is part of GNU DataExplorer.

		GNU DataExplorer is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    DataExplorer is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with GNU DataExplorer.  If not, see <https://www.gnu.org/licenses/>.

Copyright (c) 2008,2009,2010,2011,2012,2013,2014,2015,2016,2017,2018,2019,2020,2021,2022,2023,2024 Winfried Bruegmann 
****************************************************************************************/
package gde.device.estner;

/** 
 * @author Winfried Brügmann 
 * Do not edit, MessageIds are generated from messages.properties 
 */ 
public class MessageIds { 

	public final static String	GDE_MSGW3400 = "GDE_MSGW3400";
	public final static String	GDE_MSGW3401 = "GDE_MSGW3401";
	public final static String	GDE_MSGI3400 = "GDE_MSGI3400";
	public final static String	GDE_MSGI3401 = "GDE_MSGI3401";
	public final static String	GDE_MSGI3402 = "GDE_MSGI3402";
	public final static String	GDE_MSGI3403 = "GDE_MSGI3403";
	public final static String	GDE_MSGI3450 = "GDE_MSGI3450";
	public final static String	GDE_MSGI3451 = "GDE_MSGI3451";
	public final static String	GDE_MSGI3452 = "GDE_MSGI3452";
	public final static String	GDE_MSGI3454 = "GDE_MSGI3454";
	public final static String	GDE_MSGI3455 = "GDE_MSGI3455";
	public final static String	GDE_MSGI3456 = "GDE_MSGI3456";
	public final static String	GDE_MSGI3457 = "GDE_MSGI3457";
	public final static String	GDE_MSGI3458 = "GDE_MSGI3458";
	public final static String	GDE_MSGI3459 = "GDE_MSGI3459";
	public final static String	GDE_MSGI3460 = "GDE_MSGI3460";
	public final static String	GDE_MSGI3461 = "GDE_MSGI3461";
	public final static String	GDE_MSGI3462 = "GDE_MSGI3462";
	public final static String	GDE_MSGI3463 = "GDE_MSGI3463";
	public final static String	GDE_MSGI3464 = "GDE_MSGI3464";
	public final static String	GDE_MSGI3465 = "GDE_MSGI3465";
	public final static String	GDE_MSGI3466 = "GDE_MSGI3466";
	public final static String	GDE_MSGI3467 = "GDE_MSGI3467";
	public final static String	GDE_MSGI3468 = "GDE_MSGI3468";
	public final static String	GDE_MSGT3400 = "GDE_MSGT3400";
	public final static String	GDE_MSGT3401 = "GDE_MSGT3401";
	public final static String	GDE_MSGT3402 = "GDE_MSGT3402";
	public final static String	GDE_MSGT3403 = "GDE_MSGT3403";
	public final static String	GDE_MSGT3404 = "GDE_MSGT3404";
	public final static String	GDE_MSGT3405 = "GDE_MSGT3405";
	public final static String	GDE_MSGT3406 = "GDE_MSGT3406";
	public final static String	GDE_MSGT3407 = "GDE_MSGT3407";
	public final static String	GDE_MSGT3410 = "GDE_MSGT3410";
	public final static String	GDE_MSGT3411 = "GDE_MSGT3411";
	public final static String	GDE_MSGT3412 = "GDE_MSGT3412";
	public final static String	GDE_MSGT3413 = "GDE_MSGT3413";
	public final static String	GDE_MSGT3420 = "GDE_MSGT3420";
	public final static String	GDE_MSGT3421 = "GDE_MSGT3421";
	public final static String	GDE_MSGT3430 = "GDE_MSGT3430";
	public final static String	GDE_MSGT3431 = "GDE_MSGT3431";
	public final static String	GDE_MSGT3432 = "GDE_MSGT3432";
	public final static String	GDE_MSGT3433 = "GDE_MSGT3433";
	public final static String	GDE_MSGT3434 = "GDE_MSGT3434";
	public final static String	GDE_MSGT3435 = "GDE_MSGT3435";
	public final static String	GDE_MSGT3436 = "GDE_MSGT3436";
	public final static String	GDE_MSGT3437 = "GDE_MSGT3437";
	public final static String	GDE_MSGT3450 = "GDE_MSGT3450";
	public final static String	GDE_MSGT3451 = "GDE_MSGT3451";
	public final static String	GDE_MSGT3452 = "GDE_MSGT3452";
	public final static String	GDE_MSGT3453 = "GDE_MSGT3453";
	public final static String	GDE_MSGT3454 = "GDE_MSGT3454";
	public final static String	GDE_MSGT3455 = "GDE_MSGT3455";
	public final static String	GDE_MSGT3456 = "GDE_MSGT3456";
	public final static String	GDE_MSGT3457 = "GDE_MSGT3457";
	public final static String	GDE_MSGT3458 = "GDE_MSGT3458";
	public final static String	GDE_MSGT3459 = "GDE_MSGT3459";
	public final static String	GDE_MSGT3460 = "GDE_MSGT3460";
	public final static String	GDE_MSGT3461 = "GDE_MSGT3461";
	public final static String	GDE_MSGT3462 = "GDE_MSGT3462";
	public final static String	GDE_MSGT3463 = "GDE_MSGT3463";
	public final static String	GDE_MSGT3464 = "GDE_MSGT3464";
	public final static String	GDE_MSGT3466 = "GDE_MSGT3466";
	public final static String	GDE_MSGT3467 = "GDE_MSGT3467";
	public final static String	GDE_MSGT3468 = "GDE_MSGT3468";
	public final static String	GDE_MSGT3469 = "GDE_MSGT3469";
	public final static String	GDE_MSGT3470 = "GDE_MSGT3470";
	public final static String	GDE_MSGT3471 = "GDE_MSGT3471";
	public final static String	GDE_MSGT3472 = "GDE_MSGT3472";
	public final static String	GDE_MSGT3473 = "GDE_MSGT3473";
	public final static String	GDE_MSGT3474 = "GDE_MSGT3474";
	public final static String	GDE_MSGT3475 = "GDE_MSGT3475";
	public final static String	GDE_MSGT3476 = "GDE_MSGT3476";
	public final static String	GDE_MSGT3477 = "GDE_MSGT3477";
	public final static String	GDE_MSGT3478 = "GDE_MSGT3478";
	public final static String	GDE_MSGT3479 = "GDE_MSGT3479";
	public final static String	GDE_MSGT3480 = "GDE_MSGT3480";
	public final static String	GDE_MSGT3481 = "GDE_MSGT3481";
	public final static String	GDE_MSGT3482 = "GDE_MSGT3482";
	public final static String	GDE_MSGT3483 = "GDE_MSGT3483";
	public final static String	GDE_MSGT3484 = "GDE_MSGT3484";
	public final static String	GDE_MSGT3499 = "GDE_MSGT3499";

 }
