//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.1.5-b01-fcs 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2023.03.29 at 04:00:35 pm CEST 
//


package gde.device.estner;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.CollapsedStringAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{}SettingType"/>
 *         &lt;element ref="{}Channel"/>
 *         &lt;element ref="{}AccuTyp"/>
 *         &lt;element ref="{}CurrentMode"/>
 *         &lt;element ref="{}Amount"/>
 *         &lt;element ref="{}Capacity"/>
 *         &lt;element ref="{}CellCount"/>
 *         &lt;element ref="{}Program"/>
 *         &lt;element ref="{}Cycle"/>
 *         &lt;element ref="{}ChargeMode"/>
 *         &lt;element ref="{}ChargeStopMode"/>
 *         &lt;element ref="{}ChargeCurrent"/>
 *         &lt;element ref="{}DisChargeCurrent"/>
 *       &lt;/sequence>
 *       &lt;attribute name="Name" use="required" type="{http://www.w3.org/2001/XMLSchema}anySimpleType" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "settingType",
    "channel",
    "accuTyp",
    "currentMode",
    "amount",
    "capacity",
    "cellCount",
    "program",
    "cycle",
    "chargeMode",
    "chargeStopMode",
    "chargeCurrent",
    "disChargeCurrent"
})
@XmlRootElement(name = "Setting")
public class Setting {

    @XmlElement(name = "SettingType", required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "NCName")
    protected String settingType;
    @XmlElement(name = "Channel", required = true)
    protected Integer channel;
    @XmlElement(name = "AccuTyp", required = true)
    protected Integer accuTyp;
    @XmlElement(name = "CurrentMode", required = true)
    protected Integer currentMode;
    @XmlElement(name = "Amount", required = true)
    protected Integer amount;
    @XmlElement(name = "Capacity", required = true)
    protected Integer capacity;
    @XmlElement(name = "CellCount", required = true)
    protected Short cellCount;
    @XmlElement(name = "Program", required = true)
    protected Integer program;
    @XmlElement(name = "Cycle", required = true)
    protected Integer cycle;
    @XmlElement(name = "ChargeMode", required = true)
    protected Integer chargeMode;
    @XmlElement(name = "ChargeStopMode", required = true)
    protected Integer chargeStopMode;
    @XmlElement(name = "ChargeCurrent", required = true)
    protected Integer chargeCurrent;
    @XmlElement(name = "DisChargeCurrent", required = true)
    protected Integer disChargeCurrent;
    @XmlAttribute(name = "Name", required = true)
    @XmlSchemaType(name = "anySimpleType")
    protected String name;

    private static String[] tnsTbl = { "0", "1", "2", "3", "4", "5", "6", "7", "8", "9", ":", ";", "<", "=", ">", "?"};
    
    /**
     * Gets the value of the settingType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSettingType() {
        return settingType;
    }

    /**
     * Sets the value of the settingType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSettingType(String value) {
        this.settingType = value;
    }

    /**
     * Gets the value of the channel property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getChannel() {
        return channel;
    }

    /**
     * Sets the value of the channel property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setChannel(Integer value) {
        this.channel = value;
    }

    /**
     * Gets the value of the accuTyp property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getAccuTyp() {
        return accuTyp;
    }

    /**
     * Sets the value of the accuTyp property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setAccuTyp(Integer value) {
        this.accuTyp = value;
    }

    /**
     * Gets the value of the currentMode property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getCurrentMode() {
        return currentMode;
    }

    /**
     * Sets the value of the currentMode property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setCurrentMode(Integer value) {
        this.currentMode = value;
    }

    /**
     * Gets the value of the amount property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getAmount() {
        return amount;
    }

    /**
     * Gets the value of the amount property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public String getAmountPrepared() {
      return String.format("%s%s%s%s", tnsTbl[(amount & 0x00F0) >> 4], tnsTbl[amount & 0x000F], tnsTbl[(amount & 0xF000) >> 12], tnsTbl[(amount & 0x0F00) >> 8]);
    }

    /**
     * Sets the value of the amount property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setAmount(Integer value) {
        this.amount = value;
    }

    /**
     * Gets the value of the capacity property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getCapacity() {
        return capacity;
    }

    /**
     * Gets the value of the capacity property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public String getCapacityPrepared() {
        return String.format("%s%s%s%s", tnsTbl[(capacity & 0x00F0) >> 4], tnsTbl[capacity & 0x000F], tnsTbl[(capacity & 0xF000) >> 12], tnsTbl[(capacity & 0x0F00) >> 8]);
    }

    /**
     * Sets the value of the capacity property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setCapacity(Integer value) {
        this.capacity = value;
    }

    /**
     * Gets the value of the cellCount property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Short getCellCount() {
        return cellCount;
    }

    /**
     * Gets the value of the cellCount property prepared for transfer.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public String getCellCountPrepared() {
    	return String.format("%s%s", tnsTbl[(cellCount & 0xF0) >> 4], tnsTbl[cellCount & 0x0F]);
    }

    /**
     * Sets the value of the cellCount property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setCellCount(Short value) {
        this.cellCount = value;
    }

    /**
     * Gets the value of the program property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getProgram() {
        return program;
    }

    /**
     * Sets the value of the program property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setProgram(Integer value) {
        this.program = value;
    }

    /**
     * Gets the value of the cycle property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getCycle() {
        return cycle;
    }

    /**
     * Gets the value of the cellCount property prepared for transfer.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public String getCyclePrepared() {
    	return String.format("%s%s", tnsTbl[(cycle & 0xF0) >> 4], tnsTbl[cycle & 0x0F]);
    }

    /**
     * Sets the value of the cycle property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setCycle(Integer value) {
        this.cycle = value;
    }

    /**
     * Gets the value of the chargeMode property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getChargeMode() {
        return chargeMode;
    }

    /**
     * Sets the value of the chargeMode property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setChargeMode(Integer value) {
        this.chargeMode = value;
    }

    /**
     * Gets the value of the chargeStopMode property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getChargeStopMode() {
        return chargeStopMode;
    }

    /**
     * Sets the value of the chargeStopMode property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setChargeStopMode(Integer value) {
        this.chargeStopMode = value;
    }

    /**
     * Gets the value of the chargeCurrent property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getChargeCurrent() {
        return chargeCurrent;
    }

    /**
     * Gets the value of the chargeCurrent property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public String getChargeCurrentPrepared() {
      return String.format("%s%s%s%s", tnsTbl[(chargeCurrent & 0x00F0) >> 4], tnsTbl[chargeCurrent & 0x000F], tnsTbl[(chargeCurrent & 0xF000) >> 12], tnsTbl[(chargeCurrent & 0x0F00) >> 8]);
    }

    /**
     * Sets the value of the chargeCurrent property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setChargeCurrent(Integer value) {
        this.chargeCurrent = value;
    }

    /**
     * Gets the value of the disChargeCurrent property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getDisChargeCurrent() {
        return disChargeCurrent;
    }

    /**
     * Gets the value of the disChargeCurrent property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public String getDisChargeCurrentPrepared() {
      return String.format("%s%s%s%s", tnsTbl[(disChargeCurrent & 0x00F0) >> 4], tnsTbl[disChargeCurrent & 0x000F], tnsTbl[(disChargeCurrent & 0xF000) >> 12], tnsTbl[(disChargeCurrent & 0x0F00) >> 8]);
    }

    /**
     * Sets the value of the disChargeCurrent property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setDisChargeCurrent(Integer value) {
        this.disChargeCurrent = value;
    }

    /**
     * Gets the value of the name property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the value of the name property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setName(String value) {
        this.name = value;
    }

    public String toString() {
//    <Channel>1</Channel>
//    <AccuTyp>0</AccuTyp>
//    <CurrentMode>2</CurrentMode>
//    <Amount>0</Amount>
//    <Capacity>1000</Capacity>
//    <CellCount>34</CellCount>
//    <Program>2</Program>
//    <Cycle>0</Cycle>
//    <ChargeMode>0</ChargeMode>
//    <ChargeStopMode>0</ChargeStopMode>
//    <ChargeCurrent>100</ChargeCurrent>
//    <DisChargeCurrent>100</DisChargeCurrent>
    	
			//3 1 00 00 00 02 03 04 00>80 3<80 0?:0 0000 0 01 00 @
    	//  Kanal 1,2
    	//    AkkuType 0-7
    	//       Programm 0-6
    	//          Ladeart 0-7
    	//             Stromwahl 0-2
    	//                StopMode 0-4
    	//									 ZellenAnzahl 1-34
    	//                      Kapazität 
    	//                            Ladestrom
    	//                                 Entladestrom
    	//                                      Lademenge
    	//                                           Zyklusanzahl

    	return String.format("3 %d %02d %02d %02d %02d %02d %s00 %s %s %s %s %s00", getChannel(), getAccuTyp(), getProgram(), getChargeMode(), getCurrentMode(), 
    			getChargeStopMode(), getCellCountPrepared(), getCapacityPrepared(), getChargeCurrentPrepared(), getDisChargeCurrentPrepared(), getAmountPrepared(), getCyclePrepared());
    }
 }
